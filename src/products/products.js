const products = [
  {
    _id: "5b21ca3eeb7f6fbccd471815",
    name: "MODERN WALLET - ĐỨNG",
    price: 380,
    imgLink:
      "https://product.hstatic.net/1000365849/product/21_03a0de0a0a164f578b857cf0359f5b92_master.jpg",
    description:
      "Ví được làm thủ công từ chất liệu vải dệt cao cấp, mới lạ. Thiết kế ví nhỏ gọn với 5 bộ phối màu tinh tế phù hợp với nhiều kiểu trang phục và sự kiện.",
  },
  {
    _id: "5b21ca3eeb7f6fbccd471816",
    name: "CLASSIC XL WALLET",
    price: 350,
    imgLink:
      "https://product.hstatic.net/1000365849/product/11vicamelia_f8dcae1cbf914b288500034960605d70_master.jpg",
    description:
      "Dáng dấp cổ điển nhưng đã có nhiều sự cải tiến - Classic XL Wallet™ dễ dàng chinh phục tất cả bởi kích thước thoải mái để chứa đựng nhiều tiền mặt cùng phần thiết kế phân tầng thông minh và tinh xảo bên trong.",
  },
  {
    _id: "5b21ca3eeb7f6fbccd471817",
    name: "MEGA WALLET",
    price: 380,
    imgLink:
      "https://product.hstatic.net/1000365849/product/11vicamelia_e62e56c40f204b2eab61c4b60216618c_master.jpg",
    description:
      "Từ xưa đến nay, chiếc ví có thể được xem như một vật phẩm để thể hiện phong cách riêng của mỗi người. Dáng ví đứng, mỏng nhẹ giúp bạn thể hiện sự tinh tế, chất sang trọng, và lịch thiệp vốn có. ",
  },
  {
    _id: "5b21ca3eeb7f6fbccd471819",
    name: "THE TRIPLE WALLET",
    price: 330,
    imgLink:
      "https://product.hstatic.net/1000365849/product/3213123wweb_35e9dc99a8144729b002d6f102f1d154_master.jpg",
    description:
      "Từ xưa đến nay, chiếc ví có thể được xem như một vật phẩm để thể hiện phong cách riêng của mỗi người. Dáng ví đứng, mỏng nhẹ giúp bạn thể hiện sự tinh tế, chất sang trọng, và lịch thiệp vốn có. Một trong những chiếc ví có form 3 gấp đầu tiên của Camelia, Triple Wallet có hình dáng bên ngoài phối xéo bắt mắt cùng với đó là ngăn đựng tiền có khoá kéo. ",
  },
  {
    _id: "5b21a3eeb7f6fbccd47181a",
    name: "BUTTON CARD WALLET",
    price: 290,
    imgLink:
      "https://product.hstatic.net/1000365849/product/34_884a205a3d5f492f8d9d4b9bfc9ec310_master.jpg",
    description:
      " Button Card Wallet là thiết kế mang tính đột phá với chiếc nút bấm giúp cho ví được giữ chặt lại với nhau. Tạo nên cảm giác chắc chắn nhưng vẫn giữ được kích thước nhỏ gọn và đầy đủ tiện nghi.",
  },
  {
    _id: "5b21ca3eeb7f6fbcd47181a",
    name: "NANO WALLET",
    price: 240,
    imgLink:
      "https://product.hstatic.net/1000365849/product/51vicamelia_e9e5237b15b0466abd4cc70e2f38c79f_master.jpg",
    description:
      "Hãy để Nano Wallet™ sắp xếp lại sự lộn xộn của những tờ tiền mặt và thẻ nhựa trong túi giúp bạn trở nên thật phong cách và tiết kiệm được nhiều thời gian lãng phí",
  },
  {
    _id: "5b21ca3eeb7f6fbcd4718a",
    name: "THE ZIPPER WALLET",
    price: 550,
    imgLink:
      "https://product.hstatic.net/1000365849/product/11vicamelia_586943bce0ea40afa5e814c4ed680e1d_master.jpg",
    description:
      "Camelia Brand hiểu rằng việc chọn được một chiếc ví dài thanh lịch sở hữu form dáng chuẩn chỉnh vô cùng khó khăn. Vì vậy, The Zipper Wallet™ được thiết kế nhỏ vừa đủ nằm gọn trong bàn tay hoặc túi xách của bạn. ",
  },
  {
    _id: "5b2ca3eeb7f6fbccd47181a",
    name: "THE HIT WALLET",
    price: 350,
    imgLink:
      "https://product.hstatic.net/1000365849/product/21vicamelia_efe520e91abb4cc5b2fcea2bf78daa13_master.jpg",
    description:
      "The Hit Wallet™ sở hữu cho mình thiết kế nắp gập kín đáo cùng các ngăn phân tầng khéo léo là phụ kiện không thể thiếu đối với những cô nàng, chàng trai yêu thích xu hướng hiện đại và tối giản.",
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181a",
    name: "SLIM CARD WALLET",
    price: 350,
    imgLink:
      "https://product.hstatic.net/1000365849/product/14__1__fbc29f38378a47efbae4884a418e321c_master.jpg",
    description:
      "Điều làm nên chiếc Slim Wallet là thiết kế nhỏ gọn tạo nên cảm giác rất thích khi cầm trong tay. Cùng với dáng card gập gọn mang đến một chiếc ví đơn giản - nhỏ gọn - tối ưu.",
  },
];
export function getProducts() {
  return products;
}
