import React, { Component } from "react";
import RenderProducts from "./common/renderProduct";
import _ from "lodash";

const Cart = (props) => {
  const { cart, handleCartDecrement, handleCartIncrement } = props;
  console.log(cart);
  return (
    <div>
      {cart.map((product) => (
        <ul class="list-group list-group-flush col-md-8">
          <li class="list-group-item">
            <div class="d-flex justify-content-between align-items-center mt-3 p-2 items rounded">
              <div class="d-flex flex-row">
                {" "}
                <img
                  src={product.imgLink}
                  className="img-thumbnail"
                  style={{ width: "10%" }}
                />
                <div class="ml-2">
                  <span class="font-weight-bold d-block">{product.name}</span>
                  <button
                    className="btn btn-primary btn-sm m-2"
                    onClick={() => handleCartIncrement(product)}
                  >
                    +
                  </button>
                  <button
                    className="btn btn-danger btn-sm"
                    onClick={() => handleCartDecrement(product)}
                    disabled={product.quantity === 0 ? "disabled" : ""}
                  >
                    -
                  </button>
                </div>
              </div>
              <div class="d-flex flex-row align-items-center">
                <span class="d-block">{product.quantity}</span>
                <span class="d-block ml-5 font-weight-bold">
                  {product.price},000đ
                </span>
                <i class="fa fa-trash-o ml-3 text-black-50"></i>
              </div>
            </div>
          </li>
        </ul>
      ))}
    </div>
  );
};

export default Cart;
