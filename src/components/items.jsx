import { render } from "@testing-library/react";
import React, { Component } from "react";
import RenderProducts from "./common/renderProduct";
import SearchBox from "./searchBox";

const Items = (props) => {
  var { searchQuery, products, handleAddToCart, handleSearch } = props;
  if (searchQuery) {
    var productsFiltered = products.filter((m) =>
      m.name.toLowerCase().startsWith(searchQuery.toLowerCase())
    );
  }
  return (
    <div>
      <SearchBox value={searchQuery} onChange={handleSearch} />
      <RenderProducts
        products={productsFiltered ? productsFiltered : products}
        onClick={handleAddToCart}
      />
    </div>
  );
};

export default Items;
