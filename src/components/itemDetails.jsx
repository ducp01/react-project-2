import React, { Component } from "react";
class ItemDetails extends Component {
  handleSave = () => {
    // Navigate to /products
    this.props.history.push("/movies");
  };

  render() {
    return (
      <div>
        <h1>Movies Details - {this.props.match.params.id}</h1>
        <button onClick={this.handleSave}>Save</button>
      </div>
    );
  }
}

export default ItemDetails;
