import React, { Component } from "react";
const SearchBox = ({ value, onChange }) => {
  return (
    <form className="col-3 mb-3  ">
      <input
        type="text"
        name="query"
        className="form-control"
        placeholder="Search..."
        value={value}
        onChange={(e) => onChange(e.currentTarget.value)}
      />
    </form>
  );
};

export default SearchBox;
