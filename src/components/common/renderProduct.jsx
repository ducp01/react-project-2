import React, { Component } from "react";
const RenderProducts = ({ products, onClick }) => {
  return (
    <div className="album py-5 bg-light">
      <div className="container">
        <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 ">
          {products.map((product) => (
            <div className="col">
              <div className="card shadow-sm mb-2">
                <img
                  className="img-thumbnail"
                  width="100%"
                  height="200"
                  src={product.imgLink}
                  key={product._id}
                ></img>

                <div className="card-body">
                  <p className="card-text" key={product._id}>
                    {product.name}
                  </p>
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="btn-group">
                      <button
                        type="button"
                        className="btn btn-outline-secondary"
                        onClick={() => onClick(product)}
                      >
                        Add to cart
                      </button>
                    </div>
                    <small className="text-muted">{product.price},000 đ</small>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default RenderProducts;
