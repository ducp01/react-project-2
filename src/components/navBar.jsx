import { render } from "@testing-library/react";
import React, { Component } from "react";
import { getProducts } from "../products/products";
import { Link, NavLink } from "react-router-dom";

class NavBar extends Component {
  state = {
    products: getProducts(),
  };

  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link className="navbar-brand" to="/">
          Vishop
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavAltMarkup"
          aria-controls="navbarNavAltMarkup"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            <NavLink className="nav-item nav-link" to="/products">
              Products
            </NavLink>
            <NavLink className="nav-item nav-link" to="/upcomings">
              UpCommings
            </NavLink>
            <NavLink className="nav-item nav-link " to="/cart">
              Your Cart
            </NavLink>
            <NavLink className="nav-item nav-link" to="/login">
              Login
            </NavLink>
            <NavLink className="nav-item nav-link" to="/register">
              Register
            </NavLink>
          </div>
        </div>
      </nav>
    );
  }
}
export default NavBar;
