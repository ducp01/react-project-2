import logo from "./logo.svg";
import React, { Component } from "react";
import "./App.css";
import { Route, Redirect, Switch } from "react-router-dom";
import NotFound from "./components/notFound";
import NavBar from "./components/navBar";
import Items from "./components/items";
import ItemDetails from "./components/itemDetails";
import Cart from "./components/cart";
import { getProducts } from "./products/products";
import RegisterForm from "./components/registerForm";
import LoginForm from "./components/loginForm";

class App extends Component {
  state = { products: getProducts(), cart: [], searchQuery: "" };

  render() {
    return (
      <div>
        <div>
          <NavBar />
          <Switch>
            <Route path="/register" component={RegisterForm} />
            <Route path="/login" component={LoginForm} />
            <Route path="/item/?:id" component={ItemDetails} />
            <Route path="/not-found" component={NotFound} />
            <Route
              path="/cart"
              component={(props) => (
                <Cart
                  products={this.state.products}
                  cart={this.state.cart}
                  handleCartIncrement={this.handleCartIncrement}
                  handleCartDecrement={this.handleCartDecrement}
                />
              )}
            />
            <Redirect from="/products" to="/"></Redirect>
            <Route
              path="/"
              exact
              component={(props) => (
                <Items
                  products={this.state.products}
                  cart={this.state.cart}
                  searchQuery={this.state.searchQuery}
                  handleAddToCart={this.handleAddToCart}
                  handleSearch={this.handleSearch}
                  {...props}
                />
              )}
            />
            <Redirect to="/not-found"></Redirect>
          </Switch>
        </div>
      </div>
    );
  }

  handleAddToCart = (product) => {
    let cart = [...this.state.cart];
    const thisProductInCart = cart.filter((prod) => prod._id === product._id);
    if (thisProductInCart.length === 0) {
      const whatToAdd = {
        _id: product._id,
        quantity: 1,
        imgLink: product.imgLink,
        name: product.name,
        price: product.price,
      };
      cart.push(whatToAdd);
    } else {
      const index = cart.indexOf(thisProductInCart[0]);
      cart[index].quantity++;
    }
    console.log(cart);
    this.setState({ cart });
  };
  handleSearch = (query) => {
    this.setState({ searchQuery: query });
  };
  handleCartIncrement = (product) => {
    let cart = [...this.state.cart];
    const index = cart.indexOf(product);
    cart[index].quantity++;
    this.setState({ cart });
  };
  handleCartDecrement = (product) => {
    let cart = [...this.state.cart];
    const index = cart.indexOf(product);
    cart[index].quantity--;
    this.setState({ cart });
  };
}

export default App;
